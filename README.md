## About

This project is built using a mobile first responsive approach.

## Bonus
I worked on the following bonuses
- Adjust the height and spacing of the first section so that the second section is always a little visible - attracting the user to scroll
- Make it "no-js first"
- Make it accessible

## Submission URL
https://adelodundamilare.gitlab.io/eyeo/

(function () {
  
  // IF JS, HIDE MOBILE MENU
  let menuNav = document.getElementById('menuNav');
  noJsFirst()
  window.addEventListener('resize', noJsFirst)

  // TOGGLE NAV
  const navButton = document.getElementById('navButton');
  if(navButton) {
    navButton.addEventListener('click', () => {
      if(menuNav) {
        if(menuNav.hasAttribute('aria-hidden')){
          menuNav.removeAttribute('aria-hidden')
        }else {
          menuNav.setAttribute('aria-hidden', true)
        }
      }
    })
  }

  // MISC FUNCTIONS
  function noJsFirst() {
    if(menuNav && (window.innerWidth < 1000)) {
      menuNav.setAttribute('aria-hidden', true)
    }else {
      menuNav.setAttribute('aria-hidden', false)
    }
  }
})();